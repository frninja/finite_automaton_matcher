#ifndef FINITE_AUTOMATON_MATCHER_H
#define FINITE_AUTOMATON_MATCHER_H

#include <string>
#include <vector>
#include <algorithm>

class FiniteAutomatonMatcher {
public:
	void set_pattern(const std::string& pattern) {
		this->compute_fast_transition_function(pattern); // determinate automaton for O(m|sigma|)
		this->m_pattern = pattern;
	}

	template <typename PatternVisitor>
	void find_all_positions(const std::string& text, PatternVisitor& vis) {
		size_t n = text.length(), m = m_pattern.length();
		size_t q = 0; // finite automaton state
		for (size_t i = 0; i < n; ++i) {
			q = get_automaton_move(q, text[i]);
			if (q == m) {
				vis.discover_pattern(i + 1, m_pattern); // pattern discovered at i-th position
			}
		}
	}

	void find_all_positions(const std::string& text) {
		this->find_all_positions(text, null_pattern_visitor());
	}

private:
	size_t get_automaton_move(size_t state, unsigned char symb) const {
		return m_transition_function[state][symb];
	}

	void compute_prefix_function(const std::string& pattern) {
		size_t m = pattern.length();

		m_prefix_function.resize(m + 1);

		m_prefix_function[1] = 0;

		size_t k = 0;

		for (size_t q = 2; q <= m; ++q) {
			while (k > 0 && pattern[k + 1 - 1] != pattern[q - 1])
				k = m_prefix_function[k - 1];
			if (pattern[k + 1 - 1] == pattern[q - 1])
				++k;
			m_prefix_function[q] = k;
		}
	}

	void compute_fast_transition_function(const std::string& pattern) {
		size_t m = pattern.length();

		m_transition_function.resize(m + 1);
		for (auto& v : m_transition_function) {
			v.resize(K);
		}

		this->compute_prefix_function(pattern);

		for (size_t q = 0; q <= m; ++q) {
			for (size_t a = 0; a < K; ++a)
			if (q == m || (unsigned char)pattern[q + 1 - 1] != a) {
				m_transition_function[q][a] = m_transition_function[m_prefix_function[q]][a];
			}
			else
				m_transition_function[q][a] = q + 1;
		}
	}

	void compute_transition_function(const std::string& pattern) {
		size_t m = pattern.length();

		m_transition_function.resize(m + 1);
		for (auto& v : m_transition_function) {
			v.resize(K);
		}

		for (size_t q = 0; q <= m; ++q) {

			for (size_t a = 0; a < K; ++a) {
				std::string Pq_a = pattern.substr(0, q) + std::string(reinterpret_cast<char*>(&a));
				size_t k = std::min(m + 1, q + 2);
				do {
					--k;
				} while (!isSuffix(pattern.substr(0, k), Pq_a));
				m_transition_function[q][a] = k;
			}
		}
	}

	static bool isSuffix(const std::string& suffix, const std::string& text) {
		if (text.length() < suffix.length())
			return false;

		auto s_it = suffix.crbegin(), text_it = text.crbegin();
		while (s_it != suffix.crend() && text_it != text.crend()) {
			if (*s_it != *text_it)
				return false;
			++s_it; ++text_it;
		}

		return true;
	}

private:
	struct null_pattern_visitor {
		void discover_pattern(size_t i, const std::string& pattern) {
		}
	};

private:
	const static size_t K = 256; // alphabet size
	std::vector<std::vector<size_t>> m_transition_function; // [q][a]
	std::vector<size_t> m_prefix_function;
	std::string m_pattern;
};

#endif /* FINITE_AUTOMATON_MATCHER_H */