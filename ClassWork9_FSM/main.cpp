#include <iostream>
#include <locale>

#include "finite_automaton_matcher.h"
#include "finite_automaton_test.h"

using namespace std;

struct my_pattern_visitor {
	void discover_pattern(size_t i, const std::string& pattern) {
		size_t m = pattern.length();
		std::cout << "Found " << pattern << " -> " << i - m << std::endl;
	}
};

int main() {
	setlocale(LC_ALL, "");

	//std::string text = "aaababacabbababababacasds";
	//std::string pattern = "ababaca";

	//std::string text = "��������� ����������";
	//std::string pattern = "�����";

	//FiniteAutomatonMatcher fam;
	//fam.find_all_positions(text, pattern, my_pattern_visitor());

	test_speed();

	system("pause");
	return 0;
}