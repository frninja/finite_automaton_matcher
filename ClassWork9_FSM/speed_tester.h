#ifndef SPEED_TESTER_H
#define SPEED_TESTER_H

#include <functional>
#include <chrono>

struct speed_tester {
	template <typename Registrator, typename Test>
	static void single_test(Registrator& reg, Test& test) {	
		test.run(reg);
	}

};

#endif /* SPEED_TESTER_H */