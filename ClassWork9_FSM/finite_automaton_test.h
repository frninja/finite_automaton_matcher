#ifndef FINITE_AUTOMATON_TEST_H
#define FINITE_AUTOMATON_TEST_H

#include <queue>
#include <iostream>

#include <fstream>
#include <iterator>
#include <algorithm>

#include <map>

#include "finite_automaton_matcher.h"
#include "speed_tester.h"

class pattern_visitor {
public:
	pattern_visitor(const std::string& pattern) {
			m_patterns[pattern] = 0;
	}

	void discover_pattern(size_t i, const std::string& pattern) {
		if (m_patterns.find(pattern) != m_patterns.end())
			++m_patterns[pattern];
		else
			m_patterns[pattern] = 1;
	}

	void reset() {
		for (auto& kv : m_patterns)
			kv.second = 0;
	}

	void print_patterns() {
		for (auto kv : m_patterns)
			std::cout << kv.first << " -> " << kv.second << std::endl;
	}
private:
	std::map<std::string, size_t> m_patterns;
};

struct finite_automaton_test {
	template <typename PatternsContainer>
	finite_automaton_test(const std::string& pattern, const PatternsContainer& text_filenames) : m_pattern(pattern), m_pattern_visitor(pattern) {
		std::cout << "Test pattern: " << pattern << std::endl;

		std::cout << "Test text files: " << std::endl;

		for (std::string text_filename : text_filenames) {
			m_text_filenames.push(text_filename);
			std::cout << text_filename << std::endl;
		}
	};

	template <typename Registrator>
	void initialize_action(Registrator& reg) {
		m_current_text = this->load_text(m_text_filenames.front());
		reg.set_text_length(m_current_text.length());
		reg.set_text_title(m_text_filenames.front());
		m_pattern_visitor.reset();
	}

	template <typename Registrator>
	void perform_action(Registrator& reg) {
		m_finite_automaton.find_all_positions(m_current_text, m_pattern_visitor);
	}

	template <typename Registrator>
	void run(Registrator& reg) {
		m_finite_automaton.set_pattern(m_pattern);

		while (!m_text_filenames.empty()) {
			this->initialize_action(reg);

			reg.register_begin_action();

			this->perform_action(reg);

			reg.register_end_action();

			this->finalize_action(reg);
		}
	}

	template <typename Registrator>
	void finalize_action(Registrator& reg) {
		std::cout << "Tested " << m_text_filenames.front() << std::endl;
		m_pattern_visitor.print_patterns();

		m_text_filenames.pop();
	}

private:
	std::string load_text(const std::string& filename) {
		std::ifstream ifs(filename);

		std::string text((std::istreambuf_iterator<char>(ifs)),
			std::istreambuf_iterator<char>());

		return text;
	}

private:
	FiniteAutomatonMatcher m_finite_automaton; // Finite Automaton Matcher
	std::string m_pattern;
	std::queue<std::string> m_text_filenames;
	std::string m_current_text;
	pattern_visitor m_pattern_visitor;
};

struct logger {
	logger(const std::string& log_filename) : m_log(log_filename) {};

	~logger() {
		for (auto kv : m_text_length_map) {
			m_log << kv.first << ";" << kv.second.first << ";" << kv.second.second << std::endl;
		}
	}

	template <typename TimeStamp>
	void register_begin_action(const TimeStamp& begin_timestamp) {
		m_begin_timestamp = begin_timestamp;
	}

	template <typename TimeStamp>
	void register_end_action(const TimeStamp& end_timestamp) {
		m_end_timestamp = end_timestamp;

		auto test_time = (m_end_timestamp - m_begin_timestamp).count();

		//m_log << m_text_length << ";" << test_time << std::endl;
		m_text_length_map.insert(std::make_pair(m_text_length, test_time));
	}

	void register_begin_action() {
		m_begin_timestamp = std::chrono::high_resolution_clock::now();
	}

	void register_end_action() {
		m_end_timestamp = std::chrono::high_resolution_clock::now();

		auto test_time = (m_end_timestamp - m_begin_timestamp).count();

		m_text_length_map.insert(std::make_pair(m_text_length, std::make_pair(test_time, m_text_title)));
	}

	void set_text_length(size_t length) {
		m_text_length = length;
	}

	void set_text_title(const std::string& title) {
		m_text_title = title;
	}

private:
	std::ofstream m_log;
	std::chrono::time_point<std::chrono::system_clock, std::chrono::system_clock::duration> m_begin_timestamp;
	std::chrono::time_point<std::chrono::system_clock, std::chrono::system_clock::duration> m_end_timestamp;
	size_t m_text_length;
	std::string m_text_title;

	std::multimap<size_t, std::pair<long long, std::string>> m_text_length_map; // text_length -> (time, text title)
};

inline void test_speed() {
	std::vector<std::string> text_filenames = { "books/wp1.txt",
		"books/wp2.txt",
		"books/lotr1.txt",
		"books/lotr2.txt",
		"books/silb.txt",
		"books/hp1.txt",
		"books/hp2.txt",
		"books/hp3.txt",
		"books/hp4.txt",
		"books/hp5.txt",
		"books/hp6.txt",
		"books/hp7.txt" };
	std::string pattern = "������";

	logger log("output.csv");

	finite_automaton_test test(pattern, text_filenames);

	speed_tester::single_test(log, test);
}

#endif /* FINITE_AUTOMATON_TEST_H */